'use strict';
let selectedCategory = [];

// for domain wise sorting
function sortProjects() {
  console.log('clicked');
}

// for card animated description
$('.post-module').hover(function () {
  $(this)
    .find('.description')
    .stop()
    .animate(
      {
        height: 'toggle',
        opacity: 'toggle',
      },
      300
    );
});
/// for hiding contact form message
$('.message').hide();
// for converting serilized contact form dta into json object
(function ($) {
  $.fn.serializeFormJSON = function () {
    let o = {};
    let a = this.serializeArray();
    $.each(a, function () {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };
})(jQuery);
$('form').submit(function (event) {
  event.preventDefault(); //prevent default action
  let post_url = $(this).attr('action'); //get form action url
  let form_data = JSON.stringify($(this).serializeFormJSON()); //Encode form elements for submission

  console.log(form_data);
  $.ajax({
    url: post_url,
    type: 'POST',
    data: form_data,
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    success: function (response) {
      console.log(response);
      $('#contact').hide();
      $('.message').show();
    },
  });
});


// // for domain wise sorting
// $('#all').click(function () {
//   $('#all').addClass('active').siblings().removeClass('active');
//   $('.retail').show();
//   $('.bfsi').show();
//   $('.insurance').show();
//   $('.healthcare').show();
//   $('.legal').show();
//   $('.others').show();
//   console.log(selectedCategory);
//
// });
// $('#retail').click(function () {
//   $('#retail').addClass('active').siblings().removeClass('active');
//   $('.retail').show();
//   $('.bfsi').hide();
//   $('.insurance').hide();
//   $('.healthcare').hide();
//   $('.legal').hide();
//   $('.others').hide();
// });
// $('#bfsi').click(function () {
//   $('#bfsi').addClass('active').siblings().removeClass('active');
//   $('.retail').hide();
//   $('.bfsi').show();
//   $('.insurance').hide();
//   $('.healthcare').hide();
//   $('.legal').hide();
//   $('.others').hide();
// });
// $('#insurance').click(function () {
//   $('#insurance').addClass('active').siblings().removeClass('active');
//   $('.retail').hide();
//   $('.bfsi').hide();
//   $('.insurance').show();
//   $('.healthcare').hide();
//   $('.legal').hide();
//   $('.others').hide();
// });
// $('#healthcare').click(function () {
//   $('#healthcare').addClass('active').siblings().removeClass('active');
//   $('.retail').hide();
//   $('.bfsi').hide();
//   $('.insurance').hide();
//   $('.healthcare').show();
//   $('.legal').hide();
//   $('.others').hide();
// });
// $('#legal').click(function () {
//   $('#legal').addClass('active').siblings().removeClass('active');
//   $('.retail').hide();
//   $('.bfsi').hide();
//   $('.insurance').hide();
//   $('.healthcare').hide();
//   $('.legal').show();
//   $('.others').hide();
// });
// $('#others').click(function () {
//   $('#others').addClass('active').siblings().removeClass('active');
//   $('.retail').hide();
//   $('.bfsi').hide();
//   $('.insurance').hide();
//   $('.healthcare').hide();
//   $('.legal').hide();
//   $('.others').show();
// });
//
// $(".check").click(function () {
//   selectedCategory = [];
//   $.each($("input[name='category']:checked"), function () {
//     selectedCategory.push($(this).val());
//   });
//   console.log(selectedCategory);
// });

$(window).on('load', function() {
  let jQueryBridget = require('jquery-bridget');
  let Isotope = require('isotope-layout');
// make Isotope a jQuery plugin
  jQueryBridget('isotope', Isotope, $);

  let filters = [];
  let domainValue;
// init Isotope
  let $grid = $('.grid').isotope({
    itemSelector: '.grid-item',
    filter : '.grid-item'
  });

// store filter for each group
  let $checkboxes = $('.categories input');

  $('.filters').on('click', '.btn', function (event) {
    let $button = $(event.currentTarget);
    // get group key
    let $buttonGroup = $button.parents('.button-group');
    let filterGroup = $buttonGroup.attr('data-filter-group');
    filters = [];
    $checkboxes.each(function (i, elem) {
      if (elem.checked) {
        filters.push($(this).parent().attr('data-filter'));
      }
    });
    console.log(filters.length);
    if (filters.length === 0) {
      $grid.isotope({filter: domainValue});
      console.log(domainValue);
    }
    else if (domainValue === undefined) {
      $grid.isotope({filter: filters.join(',')});
    }
    else {
      $grid.isotope({filter: filters.join(domainValue + ',') + domainValue});
      console.log({filter: filters.join(domainValue + ',') + domainValue});
      console.log(filters.length);
    }
  });

// change is-checked class on buttons
  $('.btn-group').each(function (i, buttonGroup) {
    let $buttonGroup = $(buttonGroup);
    $buttonGroup.on('click', 'button', function (event) {
      $buttonGroup.find('.active').removeClass('active');
      let $button = $(event.currentTarget);
      domainValue = $(this).attr('data-filter');
      $button.addClass('active');
    });
  });
});
