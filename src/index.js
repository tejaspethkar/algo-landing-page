import 'bootstrap';
import './assets/js/argon.min';
import './assets/js/custom';
import './assets/scss/argon.scss';
import '../node_modules/particles.js/particles';
import '../node_modules/isotope-layout/dist/isotope.pkgd.min'

/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
particlesJS.load('particles-js', 'public/particles.json', function() {
  console.log('callback - particles-js config loaded');
});
// // Your jQuery code
if ($(window).width() < 470) {
  $('#sorting').addClass('btn-group-sm')
}
else {
  $('#sorting').removeClass('btn-group-sm')
}

